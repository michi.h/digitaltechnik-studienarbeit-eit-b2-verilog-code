`timescale 1ns / 1ps

module test_sim();
    reg sys_clk;
    reg [1:0]key;
    wire led_auto_gr;
    wire led_auto_r;
    wire led_auto_ge;
    wire led_fuss_r;

    ampel_fsm FSM(
            .sys_clk(sys_clk),
            .key(key),
            .led1(led_auto_gr),
            .led2(led_auto_r),
            .led_blau(led_auto_ge),
            .led_rot(led_fuss_r)
        );             
            
    initial begin 
        sys_clk  = 0;
        key = 2'b11;
        #10000 key = 2'b10;
        #2 key = 2'b11;
        #100000 key = 2'b01;
        #2 key = 2'b11;
    end 

    always #5 sys_clk = ~sys_clk;  
     
endmodule
