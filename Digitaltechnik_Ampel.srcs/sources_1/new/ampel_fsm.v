`timescale 1ns / 1ps

module ampel_fsm(
        input sys_clk,
        input [1:0]key,
        output led1,    // Auto Gr?n
        output led2,    // Auto Rot
        output led_blau,// Auto Gelb
        output led_rot  // Fu??ger Rot
    );

    /**
     *   1/3 Hz Clock
    */
    reg clk = 1'b0;
    reg[31:0] count = 32'b0;
    localparam divisor = 150000000; // 150 => T=3ns; T=3s => 150000000

    always @ (posedge(sys_clk))
    begin
        if (count == divisor - 1)
            count <= 32'b0;
        else
            count <= count + 1;
    end

    always @ (posedge(sys_clk))
    begin
        if (count == divisor - 1)
            clk <= ~clk;
        else
            clk <= clk;
    end

    // Deklaration Zustände
    localparam  S0 = 3'b000,
                S1 = 3'b001,
                S2 = 3'b010,
                S3 = 3'b011,
                S4 = 3'b100,
                S5 = 3'b101,
                S6 = 3'b110,
                S7 = 3'b111;

    reg [2:0] state = S0;


    // Zähler für State Dauer
    reg[3:0] timerCount = 4'd0;

    localparam  SEC12 = 4'd3,
                SEC24 = 4'd7;
    
    // Eingangsfunktion mit Speicherglied   
    wire in = ~(key[0] & key[1]); // Taster NC
    // input buffer
    reg buffer_in = 0;
    always @(*) begin
        if(in)
            buffer_in <= 1;
        else if(state != S0)
            buffer_in <= 0;
    end

    // Berechnung nächster Zustand
    always @(posedge clk)
    begin    
        case (state)
            S0: if(buffer_in)
                    state <= S1;
            S1: state <= S2;
            S2: state <= S3;
            S3: if(timerCount == SEC24) begin
                    state <= S4;
                    timerCount <= 0;
                end else begin
                    timerCount <= timerCount + 1;
                end
            S4: if(timerCount == SEC12) begin
                    state <= S5;
                    timerCount <= 0;
                end else begin
                    timerCount <= timerCount + 1;
                end
            S5: state <= S0;
            S6: state <= S7;
            S7: state <= S4;
        default:
            state <= S0;
        endcase
    end
    
    // Ausgangsfunktionen
    assign led1     =  ~state[2] & ~state[1] & ~state[0];
    assign led2     =   state[2] |  state[1];
    assign led_blau =  ~state[1] &  state[0];
    assign led_rot  =   state[2] | ~state[1] | ~state[0];

endmodule